<?php

namespace Swissclinic\PageOptions\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    const TABLE = 'cms_page';

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade (
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();

        if (version_compare($context->getVersion(), '1.1.0', '<')) {

            $data = [
                'cms_page' => [
                    'in_top_menu',
                    'in_footer_menu',
                    'footer_menu_order'
                ]
            ];

            foreach ($data as $table => $fields) {
                foreach ($fields as $field) {
                    $connection->dropColumn($setup->getTable($table), $field);
                }
            }

            $columns = [
                'in_header_section_one' => [
                    'type' => Table::TYPE_BOOLEAN,
                    'comment' => 'Wether the item should be display in the header section one or not.'
                ],
                'header_section_one_order' => [
                    'type' => Table::TYPE_INTEGER,
                    'comment' => 'Defines which order the items should come in.'
                ],
                'in_header_section_two' => [
                    'type' => Table::TYPE_BOOLEAN,
                    'comment' => 'Wether the item should be display in the header section two or not.'
                ],
                'header_section_two_order' => [
                    'type' => Table::TYPE_INTEGER,
                    'comment' => 'Defines which order the items should come in.'
                ],
                'in_header_section_three' => [
                    'type' => Table::TYPE_BOOLEAN,
                    'comment' => 'Wether the item should be display in the header section three or not.'
                ],
                'header_section_three_order' => [
                    'type' => Table::TYPE_INTEGER,
                    'comment' => 'Defines which order the items should come in.'
                ],
                'in_header_section_four' => [
                    'type' => Table::TYPE_BOOLEAN,
                    'comment' => 'Wether the item should be display in the header section four or not.'
                ],
                'header_section_four_order' => [
                    'type' => Table::TYPE_INTEGER,
                    'comment' => 'Defines which order the items should come in.'
                ]
            ];

            foreach ($columns as $name => $definition) {
                $connection->addColumn(self::TABLE, $name, $definition);
            }

        }

        $installer->endSetup();
    }
}
