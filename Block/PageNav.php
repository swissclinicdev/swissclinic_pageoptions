<?php
namespace Swissclinic\PageOptions\Block;

use Magento\Catalog\Model\ResourceModel\Category\Collection as CategoryCollection;
use Magento\Cms\Model\ResourceModel\Page\Collection;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;

class PageNav extends Template
{
    /**
     * @var string
     */
    protected $_template = 'Swissclinic_PageOptions::header-section-one.phtml';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Cms\Model\ResourceModel\Page\Collection
     */
    private $_pageCollection;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    private $_categoryCollection;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param Collection $pageCollection
     * @param CategoryCollection $categoryCollection
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Collection $pageCollection,
        CategoryCollection $categoryCollection,
        ScopeConfigInterface $scopeConfig,
        $data = []
    ) {
        $this->_pageCollection      = $pageCollection;
        $this->_categoryCollection  = $categoryCollection;
        $this->_scopeConfig         = $scopeConfig;
        $this->_storeManager        = $context->getStoreManager();

        parent::__construct($context, $data);
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }


    /**
     * @return Collection
     */
    public function getPages()
    {
        $collection = $this->_pageCollection;
        $collection->addFieldToFilter(
            'is_active', ['eq' => '1']
        )->getSelect()->order('header_section_one_order ASC');

        return $collection;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        $collection = $this->_categoryCollection;
        $collection->addAttributeToSelect(
            '*'
        )->addFieldToFilter(
            'children_count', ['eq' => '0']
        )->addFieldToFilter(
            'is_active', ['eq' => '1']
        )->addFieldToFilter(
            'include_in_menu', ['eq' => '1']
        )->getSelect();

        return $collection;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getConfig(string $key)
    {
        if ($key === 'phone') {
            $config = 'general/store_information/phone';
        } else {
            return null;
        }

        return $this->_scopeConfig->getValue(
            $config, ScopeInterface::SCOPE_STORES
        );
    }
}
